package go.corporation.cus.findapps.Modal;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class MBarang extends MBase{
    @SerializedName("id")
    private String id;
    @SerializedName("nama")
    private String nama;
    @SerializedName("foto")
    private String foto;
    @SerializedName("username")
    private String username;
    @SerializedName("userID")
    private String userID;
    @SerializedName("deskripsi")
    private String deskripsi;


    public Mbarang(){}

    public MBarang(String id, String nama, String foto, String username, String userID, String deskripsi) {
        this.id = id;
        this.nama = nama;
        this.foto = foto;
        this.username = username;
        this.userID = userID;
        this.deskripsi = deskripsi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
