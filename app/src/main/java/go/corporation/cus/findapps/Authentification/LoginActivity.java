package go.corporation.cus.findapps.Authentification;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import go.corporation.cus.findapps.MenuUtama.MenuActivity;
import go.corporation.cus.findapps.Modal.MUser;
import go.corporation.cus.findapps.Parent.BaseActivity;
import go.corporation.cus.findapps.R;
import id.flwi.util.ActivityUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.et_email)
    EditText mEmailEditText;
    @BindView(R.id.et_password)
    EditText mPasswordEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_login)
    public void onclick(){
        startActivity(new Intent(LoginActivity.this, MenuActivity.class));
    }
    // TODO(01) API MATI
    /* public void actionLogin(){
        if (TextUtils.isEmpty(mEmailEditText.getText())) {
            mEmailEditText.setError("Email tidak boleh kosong");
            mEmailEditText.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(mPasswordEditText.getText())) {
            mPasswordEditText.setError("Password tidak boleh kosong");
            mPasswordEditText.requestFocus();
            return;
        }

        authLogin(mEmailEditText.getText().toString(), mPasswordEditText.getText().toString());
    }
    */

    @OnClick(R.id.tvRegister)
    public void actionRegister(){
        startActivity(new Intent(LoginActivity.this, RegisterAcitvity.class));
    }
    public void authLogin(String username, String password) {
        progressDialog.show();
        Call<MUser> callUser = apiService.login(username, password);
        callUser.enqueue(new Callback<MUser>() {
            @Override
            public void onResponse(Call<MUser> call, Response<MUser> response) {
                progressDialog.dismiss();
                if(response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        if (response.body().getData() != null) {
                            setDataUser(response.body().getData());
                            startActivity(new Intent(LoginActivity.this, MenuActivity.class));
                            finish();
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, String.valueOf(response.body().getMessage()), Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(LoginActivity.this, getString(R.string.error_server), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<MUser> call, Throwable t) {
                progressDialog.dismiss();
                messsageErrorServer(t);
            }
        });
    }

    public void setDataUser(MUser.User user) {
        if (user != null) {
            if (user.getId() != null)
                ActivityUtil.setSharedPreference(this, USER_ID, user.getId());
            if (user.getUsername() != null)
                ActivityUtil.setSharedPreference(this, USER_NAME, user.getUsername());
            if (user.getToken() != null)
                ActivityUtil.setSharedPreference(this, USER_TOKEN, user.getToken());
        }
    }
}
