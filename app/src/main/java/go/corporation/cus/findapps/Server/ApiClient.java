package go.corporation.cus.findapps.Server;


import go.corporation.cus.findapps.Constants.Constants;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient implements Constants{
    private static Retrofit mRetrofit = null;
    public static ApiInterface getClient(){
        if(mRetrofit == null){
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return mRetrofit.create(ApiInterface.class);

    }
}
