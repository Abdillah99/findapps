package go.corporation.cus.findapps.Constants;

/**
 * Created by Remap on 22/01/18.
 */

public interface Constants {

    public static final String BASE_URL = "http://192.168.1.65/barang-hilang/advanced/api/web/user/";

    public static String USER_TOKEN = "userToken";
    public static String USER_EMAIL = "userEmail";
    public static String USER_ID = "userId";
    public static String USER_NAME = "userName";

}
