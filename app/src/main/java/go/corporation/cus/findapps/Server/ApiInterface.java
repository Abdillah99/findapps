package go.corporation.cus.findapps.Server;

import go.corporation.cus.findapps.Modal.MUser;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("register")
    Call<MUser> register(@Field("fullname") String fullname,
                                  @Field("password") String password,
                                  @Field("email") String email,
                                  @Field("phone") String phone,
                                  @Field("ktp") String ktp);
    @FormUrlEncoded
    @PUT("tb_user")
    Call<MUser> putUser(@Field("username") String username,
                                 @Field("password") String password,
                                 @Field("email") String email);

    @FormUrlEncoded
    @POST("login")
    Call<MUser> login(@Field("email") String email,
                             @Field("password") String password);

}
