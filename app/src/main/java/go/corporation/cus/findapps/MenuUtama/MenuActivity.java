package go.corporation.cus.findapps.MenuUtama;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import go.corporation.cus.findapps.Adapter.AdapterViewPagerNavigation;
import go.corporation.cus.findapps.R;

public class MenuActivity extends AppCompatActivity     {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    @BindView(R.id.tab)
    TabLayout mTabs;

    AdapterViewPagerNavigation mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
        initViewPager();
    }

    public void initViewPager(){
        mAdapter = new AdapterViewPagerNavigation(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mTabs.setupWithViewPager(mViewPager);
        setupTabIcons();

    }

    private void setupTabIcons(){
        mTabs.getTabAt(0).setIcon(R.drawable.ic_navigation_home);
        mTabs.getTabAt(1).setIcon(R.drawable.ic_navigation_kategori);
        mTabs.getTabAt(2).setIcon(R.drawable.ic_navigation_add);
        mTabs.getTabAt(3).setIcon(R.drawable.ic_navigation_profil);
        mTabs.getTabAt(4).setIcon(R.drawable.ic_navigation_pendapatan);
    }


}
