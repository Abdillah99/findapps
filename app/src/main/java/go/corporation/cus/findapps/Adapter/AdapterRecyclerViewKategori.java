package go.corporation.cus.findapps.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import go.corporation.cus.findapps.Modal.MBarang;
import go.corporation.cus.findapps.Modal.MKategori;
import go.corporation.cus.findapps.R;

import butterknife.BindView;

public class AdapterRecyclerViewKategori extends RecyclerView.Adapter<AdapterRecyclerViewKategori.ViewHolder> {
    List<MKategori> mKategoriList;

    public AdapterRecyclerViewKategori(List<MBarang> BarangList){
        mKategoriList = BarangList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tvUsernameKategori)
        TextView mTvUsernameKategori;
        @BindView(R.id.tvJudulKategori)
        TextView mTvJudulKategori;

        public ViewHolder(View v){
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_kategori, parent, false);
        ViewHolder mViewHolder = new ViewHolder(mView);
        return mViewHolder;
    }

    //TODO(03) isi nama api
    @Override
    public void onBindViewHolder(ViewHolder mViewHolder,final int position){
        mViewHolder.mTvJudulKategori.setText();
        mViewHolder.mTvUsernameKategori.setText();

    }

    @Override
    public int getItemCount(){
        return mKategoriList.size()
    }

}
