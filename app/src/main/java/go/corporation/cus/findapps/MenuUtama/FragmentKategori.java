package go.corporation.cus.findapps.MenuUtama;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;


import butterknife.BindView;
import go.corporation.cus.findapps.Adapter.AdapterRecyclerViewKategori;
import go.corporation.cus.findapps.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentKategori extends Fragment {
    Context mContext;

    @BindView(R.id.recycleKategori)
    RecyclerView mRecycleView;

    RecyclerView.Adapter mRecycleAdapter;
    RecyclerView.LayoutManager mRecycleLayoutManager;

    public FragmentKategori() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_kategori, container, false);
        initRecycleGrid();
    }

    private void initRecycleGrid(){
        mContext = getActivity();

        //jumlah card view
        mRecycleLayoutManager = new GridLayoutManager(mContext, 2);

        mRecycleView.setLayoutManager(mRecycleLayoutManager);
        mRecycleAdapter = new AdapterRecyclerViewKategori(mContext, "WOK");
        mRecycleView.setAdapter(mRecycleAdapter);
    }

}
