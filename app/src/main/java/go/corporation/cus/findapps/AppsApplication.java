package go.corporation.cus.findapps;

import android.app.Application;
import android.content.Context;

import go.corporation.cus.findapps.Server.ApiClient;
import go.corporation.cus.findapps.Server.ApiInterface;

/**
 * Created by Remap on 22/01/18.
 */

public class AppsApplication extends Application {

    private ApiInterface mAPIService;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private static AppsApplication get(Context context) {
        return (AppsApplication) context.getApplicationContext();
    }

    public static AppsApplication create(Context context) {
        return AppsApplication.get(context);
    }

    public ApiInterface getAPIService() {
        if (mAPIService == null) mAPIService = ApiClient.getClient();

        return mAPIService;
    }

    public void setAPIService(ApiInterface peopleService) {
        mAPIService = peopleService;
    }
}
