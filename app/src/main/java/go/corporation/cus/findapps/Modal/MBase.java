package go.corporation.cus.findapps.Modal;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MBase {
    @SerializedName("status")
    boolean status;
    @SerializedName("message")
    String message;


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
