package go.corporation.cus.findapps.Modal;

import com.google.gson.annotations.SerializedName;


public class MKategori extends MBase{
    @SerializedName("id")
    private String id;
    @SerializedName("nama")
    private String nama;

    public MKategori(){}

    public MKategori(String id, String nama) {
        this.id = id;
        this.nama = nama;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
