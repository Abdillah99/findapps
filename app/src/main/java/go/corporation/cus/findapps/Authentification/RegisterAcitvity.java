package go.corporation.cus.findapps.Authentification;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import go.corporation.cus.findapps.MenuUtama.MenuActivity;
import go.corporation.cus.findapps.Modal.MUser;
import go.corporation.cus.findapps.Parent.BaseActivity;
import go.corporation.cus.findapps.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import id.flwi.util.ActivityUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterAcitvity extends BaseActivity {

    @BindView(R.id.btnSignUp)Button mBtnSignUp;
    @BindView(R.id.etFullname)EditText mEtFullName;
    @BindView(R.id.etEmail)EditText mEtEmail;
    @BindView(R.id.etIdCard)EditText mEtIdCard;
    @BindView(R.id.etNumber)EditText mEtNumber;
    @BindView(R.id.etPassword)EditText mEtPassword;
    @BindView(R.id.etPasswordConf)EditText mEtpasswordConf;
    @BindView(R.id.toolbar) Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @OnClick(R.id.btnSignUp)
    public void onClick(View view){
        if (TextUtils.isEmpty(mEtFullName.getText())) {
            mEtFullName.setError("Nama tidak boleh kosong");
            mEtFullName.requestFocus();
            return;
        }else if (TextUtils.isEmpty(mEtEmail.getText())) {
            mEtEmail.setError("Email tidak boleh kosong");
            mEtEmail.requestFocus();
            return;
        }else if (TextUtils.isEmpty(mEtIdCard.getText())){
            mEtIdCard.setError("ID Card tidak boleh kosong");
            mEtIdCard.requestFocus();
            return;
        }else if(TextUtils.isEmpty(mEtNumber.getText())){
            mEtNumber.setError("Nomor tidak boleh kosong");
            mEtNumber.requestFocus();
            return;
        }else if (TextUtils.isEmpty(mEtPassword.getText()) || TextUtils.isEmpty(mEtpasswordConf.getText())){
            mEtPassword.setError("Password tidak boleh kosong");
            mEtpasswordConf.setError("Password tidak boleh kosong");
            mEtPassword.requestFocus();
            mEtpasswordConf.requestFocus();
            return;
        }else if((mEtPassword.getText().toString()) != (mEtpasswordConf.getText().toString())){
            mEtpasswordConf.setError("Password tidak sama");
            mEtpasswordConf.requestFocus();
            return;
        } else{
            registerPost();
        }

    }

    private void registerPost(){
        Call<MUser> postUserCall = apiService.register(mEtFullName.getText().toString(),
                mEtPassword.getText().toString(),
                mEtEmail.getText().toString(),
                mEtNumber.getText().toString(),
                mEtIdCard.getText().toString());
        postUserCall.enqueue(new Callback<MUser>() {
            @Override
            public void onResponse(Call<MUser> call, Response<MUser> response) {
                progressDialog.dismiss();
                if(response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        if (response.body().getData() != null) {
                            setDataUser(response.body().getData());
                            startActivity(new Intent(RegisterAcitvity.this, MenuActivity.class));
                            finish();
                        }
                    } else {
                        Toast.makeText(RegisterAcitvity.this, String.valueOf(response.body().getMessage()), Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(RegisterAcitvity.this, getString(R.string.error_server), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<MUser> call, Throwable t) {
                progressDialog.dismiss();
                messsageErrorServer(t);
            }
        });
    }

    public void setDataUser(MUser.User user) {
        if (user != null) {
            if (user.getId() != null)
                ActivityUtil.setSharedPreference(this, USER_ID, user.getId());
            if (user.getUsername() != null)
                ActivityUtil.setSharedPreference(this, USER_NAME, user.getUsername());
            if (user.getToken() != null)
                ActivityUtil.setSharedPreference(this, USER_TOKEN, user.getToken());
        }
    }
}