package go.corporation.cus.findapps;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;

import go.corporation.cus.findapps.Authentification.LoginActivity;
import go.corporation.cus.findapps.Constants.Constants;
import go.corporation.cus.findapps.MenuUtama.MenuActivity;
import id.flwi.util.ActivityUtil;

public class MainActivity extends AppCompatActivity implements Constants {
    private static int splashInterval = 3000;
    Handler mHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run(){
                //cek jika ada token maka masuk menu utama jika tidak maka ke menu login
                if (!TextUtils.isEmpty(ActivityUtil.getSharedPreferenceString(MainActivity.this, USER_TOKEN))){
                    Intent mIntent = new Intent(MainActivity.this, MenuActivity.class);
                    startActivity(mIntent);
                    finish();
            } else {
                    Intent mIntent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(mIntent);
                    finish();
                }

            }
        }, splashInterval);

    }
}
