package go.corporation.cus.findapps.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import go.corporation.cus.findapps.MenuUtama.FragmentCreate;
import go.corporation.cus.findapps.MenuUtama.FragmentHome;
import go.corporation.cus.findapps.MenuUtama.FragmentKategori;
import go.corporation.cus.findapps.MenuUtama.FragmentProfile;
import go.corporation.cus.findapps.MenuUtama.FragmentPendapatan;


public class AdapterViewPagerNavigation extends FragmentPagerAdapter {
    private static int MENU = 5;
    private String menuString[] = new String[]{"Home", "Kategori", "Create", "Profile", "Pendapatan"};

    public AdapterViewPagerNavigation(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public int getCount(){
        return MENU;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return menuString[position];
    }

    @Override
    public Fragment getItem(int position){
        switch (position){
            case 0:
                return new FragmentHome();
            case 1:
                return new FragmentKategori();
            case 2:
                return new FragmentCreate();
            case 3:
                return new FragmentProfile();
            case 4:
                return new FragmentPendapatan();
        }
        return null;
    }




}
